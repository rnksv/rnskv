import Vuex from "vuex";
import Vue from "vue";
Vue.use(Vuex);

const User = {
    state: {
        login: 'Guest',
        isAuth: false,
    },
    mutations: {
        logIn(state, payloadProfile) {
            state.user = payloadProfile;
            state.isAuth = true;
        }
    },
    actions: {

    },
    getters: {

    }
};
const Room = {

};
const Game = {

};

const store = new Vuex.Store({
    modules: {
        User,
        Room,
        Game
    }
});

console.log('WARN', store.state.User.login);
export default store;