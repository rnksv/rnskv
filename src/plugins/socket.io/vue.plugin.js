import io from './vendor/socket.io-client.min.js';

export default {
    install: (Vue, options) => {
        Vue.prototype.$socket = io(options.connect || '', {
            transports: ['websocket'],
            autoConnect: false
        });
    }
};